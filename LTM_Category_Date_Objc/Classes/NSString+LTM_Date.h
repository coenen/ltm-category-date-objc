//
//  NSString+LTM_Date.h
//  LTM_Category_Date_Objc
//
//  Created by 柯南 on 2021/11/17.
//

#import <Foundation/Foundation.h>
#import "LTM_DateFormatHeader.h"

NS_ASSUME_NONNULL_BEGIN

@interface NSString (LTM_Date)
/**
 时间戳转时间
 
 @param format 转换时间格式
 */
- (NSString *)date:(DateFormatter)format;
@end

NS_ASSUME_NONNULL_END
