//
//  NSString+LTM_Date.m
//  LTM_Category_Date_Objc
//
//  Created by 柯南 on 2021/11/17.
//

#import "NSString+LTM_Date.h"
#import "NSDateFormatter+LTM_Format.h"

@implementation NSString (LTM_Date)
- (NSString *)date:(DateFormatter)format{
    NSTimeInterval time = [self doubleValue]/1000;
    NSDate *date=[NSDate dateWithTimeIntervalSince1970:time];
    
    return [[NSDateFormatter getFormatter:format] stringFromDate: date];
}
@end
