//
//  LTMAppDelegate.h
//  LTM_Category_Date_Objc
//
//  Created by coenen on 11/16/2021.
//  Copyright (c) 2021 coenen. All rights reserved.
//

@import UIKit;

@interface LTMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
