//
//  LTMViewController.m
//  LTM_Category_Date_Objc
//
//  Created by coenen on 11/16/2021.
//  Copyright (c) 2021 coenen. All rights reserved.
//

#import "LTMViewController.h"

#import <LTM_Category_Date_Objc/NSDate+LTM_String.h>
#import <LTM_Category_Date_Objc/NSString+LTM_Date.h>

@interface LTMViewController ()

@end

@implementation LTMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    NSCalendar *calendar         = [NSCalendar currentCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:2021];
    [components setMonth:12];
    [components setDay:29];
//    [components setHour:23];
//    [components setMinute:59];
//    [components setSecond:59];
//
    [components setHour:9];
    [components setMinute:9];
    [components setSecond:9];
    
    NSDate *nowDate = [calendar dateFromComponents:components];

    NSLog(@"nowDate %@",[[nowDate timeStamp] date:(DateFormatter_YMDHMS)]);
    NSLog(@"昨天开始时间 %@",[nowDate.yesterdayStart timeStamp]);
    NSLog(@"昨天结束时间 %@",[nowDate.yesterdayEnd timeStamp]);
    NSLog(@"昨天开始时间 %@",[[nowDate.yesterdayStart timeStamp] date:(DateFormatter_YMDHMS)]);
    NSLog(@"昨天结束时间 %@",[[nowDate.yesterdayEnd timeStamp] date:(DateFormatter_YMDHMS)]);
    NSLog(@"今天开始时间 %@",[nowDate.curDayStart timeStamp]);
    NSLog(@"今天结束时间 %@",[nowDate.curDayEnd timeStamp]);
    NSLog(@"今天开始时间 %@",[[nowDate.curDayStart timeStamp] date:(DateFormatter_YMDHMS)]);
    NSLog(@"今天结束时间 %@",[[nowDate.curDayEnd timeStamp] date:(DateFormatter_YMDHMS)]);
    NSLog(@"本周开始时间 %@",[nowDate.curWeekStart timeStamp]);
    NSLog(@"本周结束时间 %@",[nowDate.curWeekEnd timeStamp]);
    NSLog(@"本周开始时间 %@",[[nowDate.curWeekStart timeStamp] date:(DateFormatter_YMDHMS)]);
    NSLog(@"本周结束时间 %@",[[nowDate.curWeekEnd timeStamp] date:(DateFormatter_YMDHMS)]);
    NSLog(@"本月开始时间 %@",[nowDate.curMonthStart timeStamp]);
    NSLog(@"本月结束时间 %@",[nowDate.curMonthEnd timeStamp]);
    NSLog(@"本月开始时间 %@",[[nowDate.curMonthStart timeStamp] date:(DateFormatter_YMDHMS)]);
    NSLog(@"本月结束时间 %@",[[nowDate.curMonthEnd timeStamp] date:(DateFormatter_YMDHMS)]);

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
