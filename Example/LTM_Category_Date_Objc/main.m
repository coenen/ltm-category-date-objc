//
//  main.m
//  LTM_Category_Date_Objc
//
//  Created by coenen on 11/16/2021.
//  Copyright (c) 2021 coenen. All rights reserved.
//

@import UIKit;
#import "LTMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LTMAppDelegate class]));
    }
}
